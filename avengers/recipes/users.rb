user node[:group][:user][:name] do
  comment 'nache'
  uid '1234'
  home "/home/#{node[:group][:user][:name]}"
  shell '/bin/bash'
  password node[:group][:user][:password]
end

group node[:group][:name] do
  action :create
  members node[:group][:user][:name]
  append true
end
